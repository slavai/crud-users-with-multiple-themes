<?php

$mainTheme = 'main';

return [

    'mainTheme' => $mainTheme,
    'currentTheme' => $mainTheme,

    /*
     *  key is domain, value is theme
     */
    'themes' => [
        'http://localhost:8000/'   =>  'second',
        'users-crud-test'  =>  $mainTheme
    ],


];