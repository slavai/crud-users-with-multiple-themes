@extends($mainTheme.'.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="GET" action="{{ url('/users') }}">
                            <label for="q" class="col-md-2 control-label">Search user</label>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input id="q" type="text" class="form-control" name="q" value="{{ $q }}">
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    Search
                                </button>
                            </div>
                        </form>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Country</th>
                                <th>Timezone</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->country }}</td>
                                    <td>{{ $user->timezone }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $users->appends(Request::only('q'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection