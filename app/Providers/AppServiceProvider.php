<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        $themes = Config::get('themes.themes');
        $host = $request->getHost();
        $keys = preg_grep("/{$host}/", array_keys($themes));
        if ($keys) {
            $key = array_shift($keys);
            $theme = $themes[$key];
            session(['currentTheme'=> $theme]);
        }
        $mainTheme = Config::get('themes.mainTheme') . '/views';
        View::share('mainTheme', $mainTheme);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
