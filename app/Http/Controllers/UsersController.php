<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class UsersController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index(Request $request) {

        $q = $request->get('q');
        if ($q) {
            $users = User::where('name', 'like', '%' . $q . '%')
                ->orWhere(['email' => $q])->paginate(15);
        } else {
            $users = User::paginate(15);
        }

//        dd($this->currentTheme);

        return view($this->currentTheme . '.users.index', [
            'users' => $users,
            'q' => $q,
        ]);
    }

}
