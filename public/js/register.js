$(document).ready(function () {
    var tz = jstz.determine();
    var timezone = tz.name();
    $("#timezone").val(timezone);
});